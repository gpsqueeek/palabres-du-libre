# Palabres du Libre

## Description
Le projet Palabres du Libre fournit un kit modulaire pour créer des bannières pour un stand parlant du libre en général : logiciels libres évidemment, mais aussi culture en licence libre, vie privée sur Internet, ou d'autres outils partageant cette philosophie.
Il existe deux types de bannières : les bannières titres (deux fois plus grandes) et les bannières de détail abordant une thématique générale. Le but est d'être visibles de loin plutôt que d'expliquer les concepts : il est souvent préférable d'expliquer de vive voix, quitte à avoir recours à des supports visuels supplémentaires (pour l'instant cela ne fait pas partie de ce projet, mais ça pourrait venir). Il est possible d'assembler les bannières entre elles afin de composer son stand.

## Visuels

### Bannières titres

#### Titre 1
![Titre 1](/miniatures/palabres_du_libre_titre_1.png)

#### Titre 2
![Titre 2](/miniatures/palabres_du_libre_titre_2.png)

### Bannières détails

#### Général
![Général](/miniatures/palabres_du_libre_details_general.png)

#### Partage
![Partage](/miniatures/palabres_du_libre_details_partage.png)

#### Online
![Online](/miniatures/palabres_du_libre_details_online.png)

#### Collaboration
![Collaboration](/miniatures/palabres_du_libre_details_collaboration.png)

#### Culture
![Culture](/miniatures/palabres_du_libre_details_culture.png)

#### Fun
![Fun](/miniatures/palabres_du_libre_details_fun.png)

## Utilisation
Le logiciel Gimp a été utilisé pour assembler les textes (utlisant la police de caractères Luciole, il lest recommandé de l'installer avant de générer des images si vous comptez utiliser la même police). Les fichiers sources pour Gimp sont dans le dossier src.

Pour générer une bannière de titre, il faut rendre visible un seul des deux calques du haut, selon la bannière choisie.

Pour générer une bannière de détail, il faut rendre visible un seul des dossiers dans la liste des calques, selon la bannière choisie.

Le dossier print contient les images générées pour impression des bannières sur une largeur de 61 cm.

Le dossier miniatures contient les miniatures présentées ci-dessus.

## Contributions
Ce projet a été initialement prévu comme un "one shot", mais toute contribution est la bienvenue, que ce soit sur l'aspect graphique, ou pour l'ajout de nouvelles bannières, ou d'autres outils visuels pouvant aider à un stand du Libre.

## Auteurs et remerciements
Ce projet a été développé par gpsqueeek et YannK.
Il n'aurait pas existé sans le talent incroyable de [David Revoy](https://www.davidrevoy.com/), auteur de la BD [Pepper & Carrot](https://www.peppercarrot.com/) ; tous les visuels actuellement présents viennent de son travail avec l'association [Framasoft](https://framasoft.org/), elle aussi faisant un travail essentiel sur le numérique et la culture libre. Merci à toutes et tous !

## Licence
Le projet est sous licence Creative Commons avec attribution et partage dans les mêmes conditions. 
CC-BY-SA 4.0 gpsqueeek & YannK
a derivation from original designs by David Revoy, CC-BY 4.0 
Le texte légal de la licence est disponible dansle fichier [LICENCE](https://framagit.org/gpsqueeek/palabres-du-libre/-/blob/main/LICENSE).
